package com.example.adriel.gerenceapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import ProjectsComponentsFolder.ScrumBottomNavigationViewFragment;
import ScrumComponentsFolder.MetricsScrumFragment;
import ScrumComponentsFolder.PipelineScrumFragment;
import ScrumComponentsFolder.TodoScrumFragment;
import projectsSelectionFolder.ItemProjectsAdapter;
import projectsSelectionFolder.Projects;

public class ScrumActivity extends AppCompatActivity {

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private TextView mTextMessage;
    public static int positionProjectList;
    public static Projects mainProject;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_todo:
                    fragment = new TodoScrumFragment();
                    break;
                case R.id.navigation_todo_metrics:
                    fragment = new MetricsScrumFragment();
                    break;
                case R.id.navigation_todo_pipeline:
                    fragment = new PipelineScrumFragment();
                    break;
            }
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.contentScrum, fragment).commit();
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrum);

        positionProjectList = ItemProjectsAdapter.positionMainProjects;
        mainProject = ItemProjectsAdapter.projects.get(positionProjectList);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigationScrum);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager = getSupportFragmentManager();

        fragment = new TodoScrumFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.contentScrum, fragment).commit();


    }



}
