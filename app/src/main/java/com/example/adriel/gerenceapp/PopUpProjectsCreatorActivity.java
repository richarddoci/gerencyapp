package com.example.adriel.gerenceapp;

import android.app.DatePickerDialog;
import java.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;

import ProjectsComponentsFolder.ProjectsCategory;
import projectsSelectionFolder.Projects;

public class PopUpProjectsCreatorActivity extends AppCompatActivity {

    EditText newProject, dueDate;
    Spinner  categoriesProject;
    Button   newProjectButton;

    int year, month, day;
    ProjectsCategory category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_projects_creator);

        newProject          = (EditText) findViewById(R.id.newProjectCreatorName);
        dueDate             = (EditText) findViewById(R.id.newProjectCreatorDate);
        categoriesProject   = (Spinner) findViewById(R.id.spinnerProjectCreator);
        newProjectButton    = (Button)  findViewById(R.id.confirmProjectButton);


        categoriesProject.setAdapter(new ArrayAdapter<ProjectsCategory>(this, android.R.layout.simple_list_item_1, ProjectsCategory.values()));

        categoriesProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = (ProjectsCategory) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dueDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                final Calendar mcurrentDate = Calendar.getInstance();
                year = mcurrentDate.get(Calendar.YEAR);
                month = mcurrentDate.get(Calendar.MONTH);
                day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(PopUpProjectsCreatorActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        String myFormat = "dd/MM/yyyy"; //In which you need put here
                        SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.getDefault());
                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH, selectedday);

                        dueDate.setText(sdformat.format(mcurrentDate.getTime()));
                    }
                },year, month, day);
                mDatePicker.setTitle(R.string.selectDate);
                mDatePicker.show();

            }
        });

        categoriesProject.setSelection (0);

        newProjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(newProject.getText().toString()) && !TextUtils.isEmpty(dueDate.getText().toString())){
                    ProjectsListActivity.projectses.add(new Projects(newProject.getText().toString(), dueDate.getText().toString(), category));
                    PopUpTaskCreatorActivity.projectsCategory = category;
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),R.string.noInputFound,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
