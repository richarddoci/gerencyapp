package com.example.adriel.gerenceapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import BrainstormSelectionFolder.Idea;

public class PopUpBrainstormCreatorActivity extends AppCompatActivity {

    EditText newIdea;
    RatingBar ratingBar;
    Button next;
    String idea = " ";
    int ratingValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_brainstorm_creator);

        next = (Button) findViewById(R.id.confirmIdeaButton);
        newIdea = (EditText) findViewById(R.id.newIdeaName);
        ratingBar = (RatingBar) findViewById(R.id.ratingBarPopUpIdea);

        ratingValue = 3;

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                idea = newIdea.getText().toString();

                if (!TextUtils.isEmpty(idea)) {
                    BrainstormActivity.list.add(new Idea(ratingValue, idea, false));
                    finish();
                } else {
                    Toast.makeText(getApplication(), R.string.noInputFound, Toast.LENGTH_SHORT).show();
                }
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingValue =  (int) rating;

            }
        });


    }
}