package com.example.adriel.gerenceapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import projectsSelectionFolder.ItemProjectsAdapter;
import projectsSelectionFolder.Projects;

public class ProjectsListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    public ItemProjectsAdapter adapter;
    public static ArrayList<Projects> projectses = new ArrayList<>();
    ActionBar actionBar;
    FloatingActionButton floatingActionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects_list);

        //Toolbar
        SetToolbar();

        //Recycler - ListView
        SetRecycler();

        floatingActionButton = (FloatingActionButton) findViewById(R.id.createNewProjectButton);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newProject = new Intent(getApplication(),PopUpProjectsCreatorActivity.class);
                startActivity(newProject);

            }
        });
        adapter.notifyDataSetChanged();

    }

    private void SetRecycler(){
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewProjects);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ItemProjectsAdapter(this, projectses);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }


    private void SetToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSelectProjects);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            actionBar = getSupportActionBar();
            actionBar.setTitle(R.string.ProjectsListActivity);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    
        adapter.notifyDataSetChanged();
    }
}
