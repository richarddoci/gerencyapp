package com.example.adriel.gerenceapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CreateNewUserActivity extends AppCompatActivity {

    public EditText name, email, phone, pass, confiPass;
    public Button check;
    Toolbar toolbar;
    ActionBar actionBar;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_user);

        mAuth = FirebaseAuth.getInstance();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null){                              //Para verificar se o tema utilizado tem actionbar
            actionBar = getSupportActionBar();
            actionBar.setTitle(R.string.createNewAccount);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        name = (EditText) findViewById(R.id.nameCreateNewUser);
        email = (EditText) findViewById(R.id.emailCreateNewUser);
        phone = (EditText) findViewById(R.id.phoneCreateNewUser);
        pass = (EditText) findViewById(R.id.passwordCreateNewUser);
        confiPass = (EditText) findViewById(R.id.confirmPasswordCreateNewUser);

        check = (Button) findViewById(R.id.confirm);

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailString = email.getText().toString().trim();
                String passwordString = pass.getText().toString().trim();
                String confirmPasswordString = confiPass.getText().toString().trim();
                String nameString = name.getText().toString().trim();
                String phoneString = phone.getText().toString().trim();

                if (!emailString.isEmpty()
                        && !passwordString.isEmpty()
                        && !confirmPasswordString.isEmpty()
                        && !nameString.isEmpty()
                        && !phoneString.isEmpty()){

                        if (passwordString.length() > 6 && passwordString.equals(confirmPasswordString)){
                            mAuth.createUserWithEmailAndPassword(emailString, passwordString)
                                    .addOnCompleteListener(CreateNewUserActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()){
                                                startActivity(new Intent(getApplication(), ProjectsListActivity.class));
                                                finish();
                                            }
                                            else {
                                                Toast.makeText(getApplicationContext(), "Erro", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.noInputFound, Toast.LENGTH_SHORT).show();
                }


            }
        });




    }

}
