package com.example.adriel.gerenceapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import BrainstormSelectionFolder.Idea;
import BrainstormSelectionFolder.ItemBrainstormAdapter;

public class BrainstormActivity extends AppCompatActivity {

    ActionBar actionBar;
    RecyclerView recyclerView;
    FloatingActionButton floatingActionButton;
    Toolbar toolbar;
    public static int numbStars = 0;

    public static ArrayList<Idea> list = new ArrayList<>();
    ItemBrainstormAdapter itemBrainstormAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brainstorm);

        Toolbar();

        Adapter();

        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButtonBrainstorm);
        floatingActionButton .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), PopUpBrainstormCreatorActivity.class));
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        itemBrainstormAdapter.notifyDataSetChanged();
    }

    private void Adapter (){


        recyclerView = (RecyclerView) findViewById(R.id.ListReciclerViewBrainStorm);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        itemBrainstormAdapter = new ItemBrainstormAdapter(list, this);

        recyclerView.setAdapter(itemBrainstormAdapter);

    }

    private void Toolbar (){
        toolbar = (Toolbar) findViewById(R.id.toolbarBrainstorm);
        setSupportActionBar (toolbar) ;

        if(getSupportActionBar()!= null){
            actionBar = getSupportActionBar();
            actionBar.setTitle(R.string.brainstorm);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_brainstorm_page, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sortByAlphabeticalOrder:
                SortByAlphabeticalOrder(list);
                return true;
            case R.id.sortByRating:
                SortByRatingValue(list);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void SortByAlphabeticalOrder(ArrayList<Idea> list){

        final Collator collator = Collator.getInstance(Locale.getDefault());

        if (!list.isEmpty()) {
            Collections.sort(list, new Comparator<Idea>() {
                @Override
                public int compare(Idea c1, Idea c2) {
                    //You should ensure that list doesn't contain null values!
                    return collator.compare(c1.getName(), c2.getName());
                }
            });
        }

        itemBrainstormAdapter.notifyDataSetChanged();

    }


    public void SortByRatingValue(final ArrayList<Idea> list) {
        Collections.sort(list, new Comparator<Idea>() {
            @Override
            public int compare(Idea o1, Idea o2) {
                return  o1.getImportance() > o2.getImportance() ? -1 :
                        o1.getImportance() < o2.getImportance() ?  1 : 0;
            }
        });

        itemBrainstormAdapter.notifyDataSetChanged();
    }
}
