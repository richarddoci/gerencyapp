package com.example.adriel.gerenceapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.icu.util.TimeZone;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Locale;

import ProjectsComponentsFolder.ProjectsCategory;
import ScrumComponentsFolder.TODOFolder.AppAndSiteCategories;
import ScrumComponentsFolder.TODOFolder.GameCategories;
import ScrumComponentsFolder.TODOFolder.Importance;
import ScrumComponentsFolder.TODOFolder.Sprints;
import ScrumComponentsFolder.TODOFolder.Tasks;
import ScrumComponentsFolder.TodoScrumFragment;
import projectsSelectionFolder.ItemProjectsAdapter;


public class PopUpTaskCreatorActivity extends AppCompatActivity {

    Button next;
    EditText titleName, dueDate, estimatedCost;
    Spinner importance, sprint, category;
    Importance importanceAdapter;
    Sprints sprints;

    AppAndSiteCategories appAndSiteCategories;
    GameCategories gameCategories;

    public static ProjectsCategory projectsCategory;

    private int day, month, year, date;
    String categoriesName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_task_creator);


        titleName = (EditText) findViewById(R.id.newTaskName);
        dueDate = (EditText) findViewById(R.id.newTaskDueDateInput);
        estimatedCost = (EditText) findViewById(R.id.estimatedCostInput);
        importance = (Spinner) findViewById(R.id.spinnerTaskCreatorImportance);
        category = (Spinner) findViewById(R.id.spinnerNewTaskCategories);
        sprint = (Spinner) findViewById (R.id.spinnerTaskCreatorSprint);

        if(projectsCategory == ProjectsCategory.GAME){
            category.setAdapter(new ArrayAdapter<GameCategories>(this, android.R.layout.simple_list_item_1, GameCategories.values()));
        } else  {
            category.setAdapter(new ArrayAdapter<AppAndSiteCategories>(this, android.R.layout.simple_list_item_1, AppAndSiteCategories.values()));
        }

        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(projectsCategory == ProjectsCategory.GAME){
                    gameCategories = (GameCategories)parent.getItemAtPosition(position);
                    categoriesName = gameCategories.getNome().toString();
                }else{
                    appAndSiteCategories = (AppAndSiteCategories) parent.getItemAtPosition(position);
                    categoriesName = appAndSiteCategories.getNome().toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        importance.setAdapter(new ArrayAdapter<Importance>(this, android.R.layout.simple_list_item_1, Importance.values()));

        importance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                importanceAdapter = (Importance) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                importanceAdapter = (Importance) parent.getSelectedItem();
            }
        });

        importance.setSelection(1);


        sprint.setAdapter(new ArrayAdapter<Sprints>(this, android.R.layout.simple_list_item_1, Sprints.values()));

        sprint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sprints = (Sprints) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                sprints = (Sprints) parent.getSelectedItem();

            }
        });

        sprint.setSelection(0);

        dueDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                final Calendar mcurrentDate = Calendar.getInstance();
                year = mcurrentDate.get(Calendar.YEAR);
                month = mcurrentDate.get(Calendar.MONTH);
                day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(PopUpTaskCreatorActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        String myFormat = "dd/MM/yyyy"; //In which you need put here
                        SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.getDefault());
                        mcurrentDate.set(Calendar.YEAR, selectedyear);
                        mcurrentDate.set(Calendar.MONTH, selectedmonth);
                        mcurrentDate.set(Calendar.DAY_OF_MONTH, selectedday);

                        dueDate.setText(sdformat.format(mcurrentDate.getTime()));
                    }
                },year, month, day);
                mDatePicker.setTitle(R.string.selectDate);
                mDatePicker.show();

            }
        });

        next = (Button) findViewById(R.id.confirmTaskButton);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(titleName.getText().toString()) &&
                    !TextUtils.isEmpty(dueDate.getText().toString()) &&
                    !TextUtils.isEmpty(estimatedCost.getText().toString())){

                        ItemProjectsAdapter.projects.get(ItemProjectsAdapter.positionMainProjects).getMainList().add(new Tasks(titleName.getText().toString(),
                                categoriesName,
                                importanceAdapter,
                                Integer.parseInt(estimatedCost.getText().toString()),
                                sprints));
                        TodoScrumFragment.scrumExpandableAdapter.notifyParentDataSetChanged(true);
                        finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.noInputFound, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
