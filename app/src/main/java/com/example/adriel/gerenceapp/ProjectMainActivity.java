package com.example.adriel.gerenceapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import ProjectsComponentsFolder.MessageBottomNavigationViewFragment;
import ProjectsComponentsFolder.MetricsBottomNavigationViewFragment;
import ProjectsComponentsFolder.PipelineBottomNavigationViewFragment;
import ProjectsComponentsFolder.ScrumBottomNavigationViewFragment;

public class ProjectMainActivity extends AppCompatActivity {

    private Fragment fragment;
    private FragmentManager fragmentManager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_metrics:
                    fragment = new MetricsBottomNavigationViewFragment();
                    break;
                case R.id.navigation_dashboard:
                    fragment = new ScrumBottomNavigationViewFragment();
                    break;
                case R.id.navigation_pipeline:
                    fragment = new PipelineBottomNavigationViewFragment();
                    break;
                case R.id.navigation_message:
                    fragment = new MessageBottomNavigationViewFragment();
                    break;
            }

            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment).commit();
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager = getSupportFragmentManager();

        fragment = new ScrumBottomNavigationViewFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();
    }

}
