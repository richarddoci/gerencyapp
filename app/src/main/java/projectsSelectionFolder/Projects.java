package projectsSelectionFolder;

import java.util.ArrayList;

import ProjectsComponentsFolder.ProjectsCategory;
import ScrumComponentsFolder.TODOFolder.Scrum;
import ScrumComponentsFolder.TODOFolder.Tasks;

/**
 * Created by Adriel on 24/08/2017.
 */

public class Projects {

    public String name;
    public String dueDate;
    public ProjectsCategory category;
    public ArrayList<Tasks> mainList = new ArrayList<>();

    public Projects(String name, String dueDate, ProjectsCategory category) {
        this.name = name;
        this.dueDate = dueDate;
        this.category = category;
    }

    public ProjectsCategory getCategory() {
        return category;
    }

    public void setCategory(ProjectsCategory category) {
        this.category = category;
    }

    public ArrayList<Tasks> getMainList() {
        return mainList;
    }

    public void setMainList(ArrayList<Tasks> mainList) {
        this.mainList = mainList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
}
