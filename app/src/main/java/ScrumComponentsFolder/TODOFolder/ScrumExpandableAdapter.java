package ScrumComponentsFolder.TODOFolder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.example.adriel.gerenceapp.R;

import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

/**
 * Created by Adriel on 31/08/2017.
 */

public class ScrumExpandableAdapter extends ExpandableRecyclerAdapter<Scrum, Tasks, ScrumViewHolder, TaskViewHolder> {



    private final LayoutInflater mInflater;
    public View scrumView, taskView;

    public ScrumExpandableAdapter(Context context, @NonNull List<Scrum> parentList) {
        super(parentList);
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ScrumViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        scrumView = mInflater.inflate(R.layout.item_scrum_parent, parentViewGroup, false);
        return new ScrumViewHolder (scrumView);
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        taskView = mInflater.inflate(R.layout.item_scrum_task_child, childViewGroup, false);
        return new TaskViewHolder (taskView);
    }


    @Override
    public void onBindChildViewHolder(@NonNull TaskViewHolder taskViewHolder, int parentPosition, int childPosition, @NonNull Tasks tasks) {
        taskViewHolder.bind(tasks);
    }

    @Override
    public void onBindParentViewHolder(@NonNull ScrumViewHolder scrumViewHolder, int parentPosition, @NonNull Scrum scrum) {
        scrumViewHolder.bind(scrum);

    }



}
