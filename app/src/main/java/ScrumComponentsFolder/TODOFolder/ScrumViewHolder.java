package ScrumComponentsFolder.TODOFolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.adriel.gerenceapp.R;

import java.util.List;

import ScrumComponentsFolder.TodoScrumFragment;

/**
 * Created by Adriel on 31/08/2017.
 */

public class ScrumViewHolder extends ParentViewHolder {

    public TextView todoName;

    @Override
    public void onClick(View v) {
        super.onClick(v);


    }

    public ScrumViewHolder(View itemView) {
        super(itemView);

        todoName = (TextView) itemView.findViewById(R.id.todoNameTitle);
    }

    public void bind (Scrum scrum) {
        todoName.setText(scrum.getName());
    }
}
