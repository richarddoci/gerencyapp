package ScrumComponentsFolder.TODOFolder;

public enum AppAndSiteCategories {

    FRONTEND("Front End",0),
    BACKEND("Back End",1),
    BUG("Bug",2),
    FEATURE("Feature",3),
    IDEAS("Ideias",4),
    DESIGN("Game Design",5),
    MARKETING("Marketing",6);

    String  nome;
    int     value;

    AppAndSiteCategories(String nome, int value) {
        this.nome = nome;
        this.value = value;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}


