package ScrumComponentsFolder.TODOFolder;

/**
 * Created by Adriel on 02/09/2017.
 */

public enum Sprints {

    BACKLOG ("Backlog", 0),
    SPRINT1 ("Sprint 1", 1),
    SPRINT2 ("Sprint 2", 2),
    SPRINT3 ("Sprint 3", 3);

    private String name;
    private int value;

    Sprints(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return name;
    }
}
