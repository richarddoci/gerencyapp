package ScrumComponentsFolder.TODOFolder;

public enum GameCategories {

    ART ("Arte",0),
    PROGRAMMING("Programação",1),
    SOUND("Som",2),
    GAMEDESIGN("Game Design",3),
    SCREENPLAY("Roteiro",4),
    MARKETING("Marketing",5);

    String  nome;
    int     value;

    GameCategories(String nome, int value) {
        this.nome = nome;
        this.value = value;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}


