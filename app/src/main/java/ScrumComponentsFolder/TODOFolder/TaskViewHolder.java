package ScrumComponentsFolder.TODOFolder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.example.adriel.gerenceapp.R;

import java.util.Locale;

/**
 * Created by Adriel on 31/08/2017.
 */

public class TaskViewHolder extends ChildViewHolder {

    public TextView categoryName, duration, taskName, status, numberItem;
    public ImageButton settings;
    public ImageView moodUrgency;

    public TaskViewHolder(View itemView) {
        super(itemView);

        categoryName = (TextView) itemView.findViewById(R.id.taskCategoryTextView);
        duration = (TextView) itemView.findViewById(R.id.durationTaskTextView);
        taskName = (TextView) itemView.findViewById(R.id.taskNameTextView);
        status = (TextView) itemView.findViewById(R.id.statusTaskTextView);
        numberItem = (TextView) itemView.findViewById(R.id.numberItemTaskTextView);
        settings = (ImageButton) itemView.findViewById(R.id.settingsTaskButton);
        moodUrgency = (ImageView) itemView.findViewById(R.id.moodUrgency);

    }

    public void bind (Tasks tasks){
        taskName.setText(tasks.getName().toString());
        status.setText(tasks.getStatus().toString());
        duration.setText(String.format(Locale.getDefault(), "%dh / %dh",
                tasks.getActualCost(),tasks.getEstimatedCost()));
        categoryName.setText(tasks.getCategory().toString());
    }

}
