package ScrumComponentsFolder.TODOFolder;

public class Tasks  {

    String name, category, description, dueDate;
    Importance importance = Importance.NORMAL;
    Sprints sprints = Sprints.BACKLOG;
    Status status = Status.TODO;
    int estimatedCost, actualCost = 0;
    boolean isCompleted = false;

    public Tasks() {
    }

    public Tasks(String name, String category, Importance importance, int estimatedCost, Sprints sprints) {
        this.name = name;
        this.category = category;
        this.importance = importance;
        this.estimatedCost = estimatedCost;
        this.sprints = sprints;
    }

    public Tasks(String name, String category, String dueDate, Importance importance, int estimatedCost, int actualCost) {
        this.name = name;
        this.category = category;
        this.dueDate = dueDate;
        this.importance = importance;
        this.estimatedCost = estimatedCost;
        this.actualCost = actualCost;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public Sprints getSprints() {
        return sprints;
    }

    public void setSprints(Sprints sprints) {
        this.sprints = sprints;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getActualCost() {
        return actualCost;
    }

    public void setActualCost(int actualCost) {
        this.actualCost = actualCost;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Importance getImportance() {
        return importance;
    }

    public void setImportance(Importance importance) {
        this.importance = importance;
    }

    public int getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(int estimatedCost) {
        this.estimatedCost = estimatedCost;
    }
}
