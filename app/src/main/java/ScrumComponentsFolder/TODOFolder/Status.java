package ScrumComponentsFolder.TODOFolder;

import com.example.adriel.gerenceapp.R;

public enum Status {

    TODO ("To Do", 0),
    DOING ("Doing", 1),
    DONE ("Done", 2);

    private String name;
    private int value;

    Status(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return name;
    }
}
