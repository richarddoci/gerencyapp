package ScrumComponentsFolder.TODOFolder;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adriel on 31/08/2017.
 */

public class Scrum implements Parent<Tasks> {

    private ArrayList<Tasks> taskes;
    private String name;

    public Scrum(String name, ArrayList<Tasks> taskses) {
        this.name = name;
        this.taskes = taskses;
    }


    public String getName() {
        return name;
    }

    @Override
    public ArrayList<Tasks> getChildList() {
        return taskes;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return true;
    }
}
