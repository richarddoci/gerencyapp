package ScrumComponentsFolder.TODOFolder;

import com.example.adriel.gerenceapp.R;

/**
 * Created by Adriel on 30/08/2017.
 */

public enum Importance {

    LOW ("Low", 0),
    NORMAL ("Normal", 1),
    HIGH ("High", 2),
    URGENT ("Urgent", 3);

    private String name;
    private int value;

    Importance(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return name;
    }
}
