package ScrumComponentsFolder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.adriel.gerenceapp.PopUpTaskCreatorActivity;
import com.example.adriel.gerenceapp.R;
import com.example.adriel.gerenceapp.ScrumActivity;

import java.util.ArrayList;
import java.util.Locale;


import ScrumComponentsFolder.TODOFolder.Scrum;
import ScrumComponentsFolder.TODOFolder.ScrumExpandableAdapter;
import ScrumComponentsFolder.TODOFolder.Sprints;
import ScrumComponentsFolder.TODOFolder.Tasks;
import projectsSelectionFolder.ItemProjectsAdapter;
import projectsSelectionFolder.Projects;

public class TodoScrumFragment extends Fragment {

    RecyclerView scrumRecyclerView;
    ActionBar actionBar;
    public static ScrumExpandableAdapter scrumExpandableAdapter;
    FloatingActionButton addTask;
    Sprints mainSprint;
    static int mainProjectPosition = ScrumActivity.positionProjectList;
    static Projects mainProject = ScrumActivity.mainProject;
    public static ArrayList<Scrum> scrumList = new ArrayList<>();
    public static ArrayList<Tasks> sprints = new ArrayList<>();
    public static ArrayList<Tasks> todo = new ArrayList<>();
    public static ArrayList<Tasks> doing = new ArrayList<>();
    public static ArrayList<Tasks> done = new ArrayList<>();
    public static ArrayList<Tasks> mainList = new ArrayList<>();

    public TodoScrumFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mainProjectPosition = ScrumActivity.positionProjectList;
        mainProject = ScrumActivity.mainProject;
        mainList = mainProject.getMainList();

        addTask = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonAddTask);

        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PopUpTaskCreatorActivity.class));
            }
        });

        mainSprint = Sprints.BACKLOG;

        generateScrumList();
        Adapter(view);
        SetToolbar(view);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        ClearScrum();

        switch (item.getItemId()){
            case R.id.backlog:
                mainSprint = Sprints.BACKLOG;
                break;
            case R.id.sprint1:
                mainSprint = Sprints.SPRINT1;
                break;
            case R.id.sprint2:
                mainSprint = Sprints.SPRINT2;
                break;
            case R.id.sprint3:
                mainSprint = Sprints.SPRINT3;
                break;
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }

        actionBar.setTitle(String.format(Locale.getDefault(), "%s - %s", getResources().getString(R.string.Scrum), mainSprint.toString()));
        refreshLists(mainList, mainSprint);
        scrumExpandableAdapter.notifyParentDataSetChanged(true);
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sprints_selector, menu);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_todo_scrum, container, false);
    }

    private void Adapter (View view){

        scrumRecyclerView = (RecyclerView) view.findViewById(R.id.scrumRecyclerView);
        scrumExpandableAdapter = new ScrumExpandableAdapter(view.getContext(), scrumList);
        scrumRecyclerView.setAdapter(scrumExpandableAdapter);
        scrumRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

    }


    private void SetToolbar(View view){
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbarScrum);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            actionBar.setTitle(String.format(Locale.getDefault(), "%s - %s", getResources().getString(R.string.Scrum), mainSprint.toString()));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    private void generateScrumList(){

       if (scrumList.isEmpty()){
           scrumList.add(new Scrum("To Do", todo));
           scrumList.add(new Scrum("Doing", doing));
           scrumList.add(new Scrum("Done", done));
       }
       else
       {
           mainList = mainProject.getMainList();
           refreshLists(mainList, mainSprint);
       }

        for (Scrum scrum : scrumList) {
            if (!scrum.getChildList().isEmpty())
                scrum.isInitiallyExpanded();
        }

    }

    private void refreshLists (ArrayList<Tasks> list, Sprints sprint){

        sprints.clear();

        for (Tasks t : list) {

            if (t.getSprints() == sprint){
                sprints.add(t);
            }
        }

        sortByScrumStatus(sprints);

    }

    private void sortByScrumStatus (ArrayList<Tasks> task){
        for (Tasks t : task) {
            switch (t.getStatus()){
                case TODO:
                    todo.add(t);
                    break;
                case DOING:
                    doing.add(t);
                    break;
                case DONE:
                    done.add(t);
                    break;
                default:
                    break;
            }

        }

       if (todo.isEmpty() && doing.isEmpty() && done.isEmpty()){
           Toast.makeText(getContext(), mainSprint.toString() + " " + getResources().getString(R.string.noTasks), Toast.LENGTH_SHORT).show();
       }

       ItemProjectsAdapter.projects.get(mainProjectPosition).setMainList(mainList);
       scrumExpandableAdapter.notifyParentDataSetChanged(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        ClearScrum();
        mainList = mainProject.getMainList();
        refreshLists(mainList, mainSprint);
        scrumExpandableAdapter.notifyParentDataSetChanged(true);
    }

    private void ClearScrum(){
        if (!scrumList.isEmpty()){
            for (int a = 0; a < scrumList.size(); a++){
                scrumList.get(a).getChildList().clear();
            }
        }

    }
}
