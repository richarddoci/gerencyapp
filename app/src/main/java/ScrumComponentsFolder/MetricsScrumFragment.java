package ScrumComponentsFolder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.adriel.gerenceapp.R;
import com.example.adriel.gerenceapp.ScrumActivity;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import ScrumComponentsFolder.TODOFolder.Tasks;


public class MetricsScrumFragment extends Fragment {

    public MetricsScrumFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_metrics_scrum, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PieChart pieChart = (PieChart) view.findViewById(R.id.piechartCategory);
        PieChart pieChartHours = (PieChart) view.findViewById(R.id.piechartCategoryEstimated);
        PieChart pieChartTasks = (PieChart) view.findViewById(R.id.piechartCategoryCompleted);

        SetUpPiechart(pieChart, "Categorias Abertas/Task");
        SetUpPiechartEstimated(pieChartHours, "Categorias Abertas/Horas");
        SetUpPiechartCompleted(pieChartTasks, "Categorias Fechadas/Tasks");
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void SetUpPiechart(PieChart pieChart, String message){
        List<PieEntry> pieEntries = new ArrayList<>();

        for (Tasks t : ScrumActivity.mainProject.getMainList()) {
            pieEntries.add(new PieEntry(1, t.getCategory()));
        }

        PieDataSet set = new PieDataSet(pieEntries, " ");
        PieData data = new PieData(set);


        set.setColors(ColorTemplate.MATERIAL_COLORS);
        data.setDrawValues(true);
        pieChart.getDescription().setText(message);
        set.setValueFormatter(new MyValueFormatter(" Tasks"));
        pieChart.animateXY(320, 320);
        pieChart.setData(data);
        pieChart.invalidate();
    }

    private void SetUpPiechartEstimated(PieChart pieChart, String message){
        List<PieEntry> pieEntries = new ArrayList<>();

        for (Tasks t : ScrumActivity.mainProject.getMainList()) {
            pieEntries.add(new PieEntry(t.getEstimatedCost(), t.getCategory()));
        }

        PieDataSet set = new PieDataSet(pieEntries, " ");
        PieData data = new PieData(set);


        set.setColors(ColorTemplate.MATERIAL_COLORS);
        data.setDrawValues(true);
        pieChart.getDescription().setText(message);
        set.setValueFormatter(new MyValueFormatter(" h"));
        pieChart.animateY(320);
        pieChart.setData(data);
        pieChart.invalidate();
    }

    private void SetUpPiechartCompleted(PieChart pieChart, String message){
        List<PieEntry> pieEntries = new ArrayList<>();

        for (Tasks t : ScrumActivity.mainProject.getMainList()) {
            int a = 0;

            for (Tasks s : ScrumActivity.mainProject.getMainList()){
                if (s.isCompleted()){
                    a++;
                }
            }

            for (PieEntry p : pieEntries) {
                if (!p.getLabel().equals(t.getCategory())){
                    pieEntries.add(new PieEntry(a, t.getCategory()));

                }
            }
        }

        PieDataSet set = new PieDataSet(pieEntries, " ");
        PieData data = new PieData(set);


        set.setColors(ColorTemplate.MATERIAL_COLORS);
        data.setDrawValues(true);
        pieChart.getDescription().setText(message);
        set.setValueFormatter(new MyValueFormatter());
        pieChart.animateY(320);
        pieChart.setData(data);
        pieChart.invalidate();
    }

    class MyValueFormatter implements IValueFormatter {

        private DecimalFormat mFormat;
        String message;

        MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }

        MyValueFormatter(String message) {
            mFormat = new DecimalFormat("###,###,##0");
            this.message = message;
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            // write your logic here
            return mFormat.format(value) + message; // e.g. append a dollar-sign
        }
    }
    


}

