package BrainstormSelectionFolder;

import android.support.annotation.NonNull;

public class Idea {

    //Nome, importâcia, selecionado

    int     importance;
    String  name;
    boolean selected;

    public Idea(int importance, String name, boolean selected) {
        this.importance = importance;
        this.name = name;
        this.selected = selected;
    }

    public Idea() {
    }

    public int getImportance() {
        return importance;
    }

    public void setImportance(int importance) {
        this.importance = importance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
