package BrainstormSelectionFolder;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.adriel.gerenceapp.R;

import java.util.ArrayList;
import java.util.List;

public class ItemBrainstormAdapter extends RecyclerView.Adapter <ItemBrainstormAdapter.IdeasListHolder>{

    List<Idea> ideas = new ArrayList<>();

    private LayoutInflater inflater;
    Context context;

    public ItemBrainstormAdapter(List<Idea> ideas, Context context) {
        this.ideas = ideas;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public IdeasListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.items_brainstorm_list,parent,false);
        IdeasListHolder ideasListHolder = new IdeasListHolder(view);
        return ideasListHolder;
    }

    @Override
    public void onBindViewHolder(final IdeasListHolder holder, final int position) {
        holder.name.setText(ideas.get(position).getName());
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoveItem(position);
            }
        });
        holder.importance.setRating(ideas.get(position).getImportance());
    }

    @Override
    public int getItemCount() {
        return ideas.size();
    }

    public ItemBrainstormAdapter(List<Idea> ideas, LayoutInflater inflater, Context context) {
        this.ideas = ideas;
        this.inflater = inflater;
        this.context = context;
    }

    public class IdeasListHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageButton del;
        RatingBar importance;

        public IdeasListHolder(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.itemNameBrainstormList);
            del = (ImageButton) itemView.findViewById(R.id.deleteButtonBrainstormList);
            importance = (RatingBar) itemView.findViewById(R.id.ratingBarBrainstormList);
        }

    }

    private void UpdateList(Idea idea) {
        InsertItem(idea);
    }

    // Método responsável por inserir um novo usuário na lista e notificar que há novos itens.
    public void InsertItem(Idea idea) {
        ideas.add(idea);
        notifyItemInserted(getItemCount());
    }

    public void UpdateItem(int position) {
        Idea proj = ideas.get(position);
        //Changes...  proj.setAlgumaCoisa
        notifyItemChanged(position);
    }

    public void RemoveItem(int position) {
        ideas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, ideas.size());
    }
}
