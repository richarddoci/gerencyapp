package ProjectsComponentsFolder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.adriel.gerenceapp.BrainstormActivity;
import com.example.adriel.gerenceapp.R;
import com.example.adriel.gerenceapp.ScrumActivity;

public class ScrumBottomNavigationViewFragment extends Fragment {

    //ImageButton finzao;
    ImageButton brainstormButton, todoButton;

    public ScrumBottomNavigationViewFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0.5f);

        finzao = (ImageButton)view.findViewById(R.id.imageFinish);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        finzao.setColorFilter(filter);*/

        brainstormButton = (ImageButton)view.findViewById(R.id.imageBrainstorm);
        brainstormButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BrainstormActivity.class));

            }
        });
        
        todoButton = (ImageButton) view.findViewById(R.id.imageScrum);
        todoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ScrumActivity.class));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scrum_bottom_navigation_view, container, false);
    }


}
