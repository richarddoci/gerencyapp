package ProjectsComponentsFolder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adriel.gerenceapp.R;

public class MetricsBottomNavigationViewFragment extends Fragment {

    public MetricsBottomNavigationViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_metrics_bottom_navigation_view, container, false);
    }

}
