package ProjectsComponentsFolder;


public enum ProjectsCategory {

    APP ("Aplicativo",0),
    GAME("Jogo",1),
    SITE("Site",2);

    private String  nome;
    private int     value;

    @Override
    public String toString() {
        return nome;
    }

    ProjectsCategory(String nome, int value) {
        this.nome = nome;
        this.value = value;
    }
}
